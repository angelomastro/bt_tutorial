
#include <yarp/os/Network.h>
#include <yarp/os/Port.h>
#include <yarp/os/Time.h>
#include <yarp/os/Property.h>

#include <Command.h>

#include <future>
#include <iostream>


int logic_sequence(Command& left_node,
                   Command& right_node,
                   std::string const& text)
{
    std::cout << "---- sequence -----" << std::endl;
//    std::cout << "searching for '" << text << "' (left node) ... " << std::endl;
    left_node.find_text(text);
    if (left_node.status() == MsgType::SUCCESS)
    {
//        std::cout << "found on left!" << std::endl;
//        std::cout << "moving on to the next node " << std::endl;
//        std::cout << "searching for '" << text 
//                  << "' (right node) ... " << std::endl;
        right_node.find_text(text);
        if (right_node.status() == MsgType::SUCCESS)
        {
//            std::cout << "found on right!" << std::endl;
            return 0;
        }   
        else
        {
//            std::cout << "not found on right" << std::endl;
            return 2;
        }
    }
    
//    std::cout << "not found on left" << std::endl;
    return 1;
}


int logic_fallback(Command& left_node,
                   Command& right_node,
                   std::string const& text)
{
    std::cout << "---- fallback -----" << std::endl;
//    std::cout << "searching for '" << text << "' (left node) ... " << std::endl;
    left_node.find_text(text);
    if (left_node.status() == MsgType::SUCCESS)
    {
//        std::cout << "found!" << std::endl;
        return 0;
    }   
    else
    {
//        std::cout << "not found " << std::endl;
//        std::cout << "fallback for '" << text 
//                  << "' (right node) ... " << std::endl;
        right_node.find_text(text);
        if (right_node.status() == MsgType::SUCCESS)
        {
//            std::cout << "found!" << std::endl;
            return 0;
        }   
        else
        {
//            std::cout << "not found " << std::endl;
            return 3;
        }
    }
    return 0;
}

bool find_text(Command& node,
                std::string const& text)
{
    node.find_text(text);
    if (node.status() == MsgType::SUCCESS)
        return true;
    return false;
}


int logic_parallel(Command& left_node,
                   Command& right_node,
                   std::string const& text)
{
    std::cout << "---- parallel -----" << std::endl;
//    std::cout << "searching for '" << text 
//              << "' in parallel (left and right nodes) ... " 
//              << std::endl;

    std::future<bool> ret_left = std::async(find_text, 
                                            std::ref(left_node), 
                                            std::ref(text));
    std::future<bool> ret_right = std::async(find_text, 
                                            std::ref(right_node),
                                            std::ref(text));

    bool r1, r2;
    r1 = ret_left.get();
    r2 = ret_right.get();
    if (r1 && r2)
    {
//        std::cout << "found on left and right!" << std::endl;
        return 0;
    }
    else if (r1 && !r2)
    {
//        std::cout << "found on left but not on right ..." << std::endl;
        return 1;
    }
    else if (!r1 && r2)
    {
//        std::cout << "found on right but not on left ..." << std::endl;
        return 2;
    }

//    std::cout << "not found on both" << std::endl;
    return 3;
}


void print_status(Command& left_node, 
                  Command& right_node)
{
    switch(left_node.status())
    {
        case MsgType::SUCCESS:
            std::cout << "left SUCCESS";
            break;
        case MsgType::RUNNING:
            std::cout << "left RUNNING";
            break;
        default:
            std::cout << "left FAILURE";
            break;
    }
    
    std::cout << " | ";
    
    switch(right_node.status())
    {
        case MsgType::SUCCESS:
            std::cout << "right SUCCESS";
            break;
        case MsgType::RUNNING:
            std::cout << "right RUNNING";
            break;
        default:
            std::cout << "right FAILURE";
            break;
    }

    std::cout << std::endl;
}

int main(int argc, char* argv[])
{
    // this client (parent node) will search for a phrase 
    // (option: --text <string to search>) in the left node
    // or fall_back on the right node

    yarp::os::Property config;
    config.fromCommand(argc, argv);
    std::string left_port = config.find("left").toString().c_str();
    std::string right_port = config.find("right").toString().c_str();

    //yarp init connection between parent and children nodes (left and right)
    yarp::os::Network network;
    
    yarp::os::Port port_child_left;
    if (!port_child_left.open("/parent_to_child_left"))
    {
        std::cerr << "failed to open /parent_to_child_left" << std::endl;
        return 1;
    }
    if (!network.connect("/parent_to_child_left", "/" + left_port))
    {
        std::cerr << "Failed to connect /parent to " 
                  << left_port << std::endl;
        return 2;
    }    
    Command left_node;
    left_node.yarp().attachAsClient(port_child_left);

    yarp::os::Port port_child_right;
    if (!port_child_right.open("/parent_to_child_right"))
    {
        std::cerr << "failed to open /parent_to_child_right" << std::endl;
        return 1;
    }
    if (!network.connect("/parent_to_child_right", "/" + right_port))
    {
        std::cerr << "Failed to connect /parent to "
                  << right_port << std::endl;
        return 2;
    }
    Command right_node;
    right_node.yarp().attachAsClient(port_child_right);


    while (true) // life cycle
    {
        left_node.start();
        right_node.start();
        yarp::os::Time::delay(3); 
        
        //command line input
        std::string node_type, text;
        std::cout << "search type: sequence (s), fallback (f), parallel (p): ";
        std::cin >> node_type;
        std::cout << "text to search: ";
        std::cin >> text;
        std::cout << std::flush;
        
        int ret = -1;
        if (node_type == "sequence" || node_type == "s")
            ret = logic_sequence(left_node, right_node, text);
        else if (node_type == "fallback" || node_type == "f")
            ret = logic_fallback(left_node, right_node, text);
        else if (node_type == "parallel" || node_type == "p")
            ret = logic_parallel(left_node, right_node, text);
        else
            std::cerr << "node type '" 
                      << node_type << "' not known. no action taken " 
                      << std::endl;
        
        print_status(left_node, right_node); 
        
        if (ret == 0)
            std::cout << "SUCCESS" << std::endl;
        else
            std::cout << "FAILURE" << std::endl;

        yarp::os::Time::delay(3);
        left_node.stop();
        right_node.stop();
        yarp::os::Time::delay(3);
    }
    
    return 0;
}
