
#include <yarp/os/Network.h>
#include <yarp/os/Port.h>
#include <yarp/os/Time.h>
#include <yarp/os/Property.h>

#include <Command.h>

#include <atomic>
#include <iostream>
#include <fstream>

class SearchService : public Command
{
public:
    virtual void start() override;
    virtual void stop() override;
    virtual MsgType status() override;
    virtual void find_text(const std::string& text) override;
    
    SearchService(std::string const& file_path);
    std::string get_status();
    
private:
    std::atomic<MsgType> mReturn; // child and father thread will use
    std::string mFilePath;
};



SearchService::SearchService(std::string const& file_path) 
: mReturn(MsgType::STOPPED), mFilePath(file_path)
{
    std::cout << "using source: " << mFilePath << std::endl;
}

void SearchService::start()
{
    mReturn = MsgType::RUNNING;
}

void SearchService::stop()
{
    mReturn = MsgType::STOPPED;
}

MsgType SearchService::status()
{
    return mReturn.load();
}

std::string SearchService::get_status()
{
    MsgType ret = mReturn.load();
    switch(ret)
    {
        case MsgType::FAILURE:
            return "FAILURE";
        case MsgType::STOPPED:
            return "STOPPED";
        case MsgType::RUNNING:
            return "RUNNING";
        case MsgType::SUCCESS:
            return "SUCCESS";
        default:
            break;
    }
    return "UNKNOWN";
}

void SearchService::find_text(std::string const& text)
{
    std::ifstream ifs(mFilePath);
    if (!ifs.is_open())
    {
        std::cerr << "source file " << mFilePath 
                  << " is not open." << std::endl;
        mReturn = MsgType::FAILURE;
        return;
    }
    
    std::cout << "searching for '" << text << "'" << std::endl;

    std::string line;
    while (std::getline(ifs, line))
        if (line.find(text) != std::string::npos)
        {
            std::cout << "found!" << std::endl;
            mReturn = MsgType::SUCCESS;
            return;
        }
    
    mReturn = MsgType::FAILURE;
    std::cout << "not found " << std::endl;
    ifs.close();
}




int main(int argc, char* argv[])
{
    yarp::os::Property config;
    config.fromCommand(argc, argv);
    std::string file_path = config.find("source").toString().c_str();
    std::string port_name = config.find("port").toString().c_str();

    yarp::os::Network network;
    yarp::os::Port port;
    
    SearchService service(file_path);
    service.yarp().attachAsServer(port);
    
    if (!port.open("/" + port_name))
    {
        std::cerr << "failed to open port " << "/" + port_name << std::endl;
        return 1;
    }

    bool update_status = false;
    std::cout << "server is running!" << std::endl;
    while(true) // life cycle: the node is alive and sleeping
    {
        while (service.status() == MsgType::RUNNING) // delivery cycle: 
        {                                      // the node is alive and working
            update_status = true;
            yarp::os::Time::delay(1);
	        std::cout << " running ... " << std::endl;
        }
        
        if (update_status) // the node reports its outcome once, then sleeps
        {
            std::cout << service.get_status() << std::endl;
            update_status = false;
        }
        
        yarp::os::Time::delay(1);
    }
    return 0;
}
