# bt_tutorial

this repo is a test to implement a 3 component binary decision tree.  it uses YARP to communicate between modules

## Building ##
Have cmake, c++11, yarp installed. 

    $ mkdir build && cd build
    $ cmake ..

## Running ##
Required: 4 terminal windows :)
 
Logic: the parent node queries the left node. if successful, returns true. If not, it falls back on the right node. If successful, returns true, else false. During all the quering time the status of the parent node is "RUNNING", else it is either "SUCCESS" or "FAILURE". 

Interactive mode: once started the parent_node, the interactive mode will ask you what type of tree (sequential, fallback, or parallel) you want to execute. Then, it will ask you what string you want to search for. 

    $ yarp server (t1)
    $ ./child_search_node --source <source_file_path> --port "<port1>" (t2)
    $ ./child_search_node --source <fallback_file_path> --port "<port2>" (t3)
    $ ./parent_node --left "<port1>" --right "<port2>" (t4)

For example: 

    $ search type: sequence (s), fallback (f), parallel (p): s
    $ text to search: "Lake Shore"

will FAIL because this phrase is only on the first source (left node)

    $ search type: sequence (s), fallback (f), parallel (p): f
    $ text to search: "Brexit"

will SUCCEED because this phrase not on the first, but is on the second source

    $ search type: sequence (s), fallback (f), parallel (p): p
    $ text to search: "a"

will SUCCEED because this letter (chances are...) is in both
