# each child sends the parent a heartbeat, and a return value
enum MsgType
{
    SUCCESS = 0,
    RUNNING = 1,
    STOPPED = 2,
    FAILURE = 3
}

# the parent sends the children specific tasks
service Command
{
    void start();     # these 2 are orders from client to server
    void stop();      
    MsgType status(); # this is a getter from client to server
    
    void find_text(1: string text); # this is a request to server
}

